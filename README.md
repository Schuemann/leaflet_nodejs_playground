# Readme for dev-Setup. Hints for developing and Setup.

- started up with this tutorial: 
 
~~~
https://www.w3schools.com/nodejs/nodejs_modules.asp
~~~

# install postgresql\
https://www.postgresql.org/

create a user "pi"\
create a database "test"\
port: 5432

# init nodejs: 

## init nodejs (Version v8.11.3) on Windows
- run "npm init" in your nodejs-directory for creating package.json
- run "npm install express"
- run "npm install pg" for adding postgresql-packages
- change the database-password in server/database.js to your chosen password for user "pi" 

## init nodejs on Windows or WSL / Subsystem

- To install nodejs, follow instructions offered here: 

~~~
https://github.com/nodesource/distributions/blob/master/README.md#debinstall
~~~

- init nodejs package module on unix / subsystem: 

~~~
$ sudo apt-get update
$ sudo apt-get install npm
~~~

- install npm express: 

~~~
$ npm install express
~~~

- add psql-packages: 

~~~
$ npm install pg
~~~

- change the database-password in server/database.js to your chosen password for user "pi" 

# Start Nodejs-Server

HINT: Do NOT add the nodejs-files to the Git-Repository.

- start server with: 

~~~
$ node server.js
~~~

# Server URL's

~~~
http://localhost:8080/
    
http://localhost:8080/eventDetails
     
http://localhost:8080/eventOverview
~~~

# Resources

#icons

~~~
https://fontawesome.com/
~~~

# URLs

- map-Api
https://leafletjs.com/reference-1.3.4.html#map-example

- Facebook-Login
https://developers.facebook.com/docs/facebook-login/web?locale=de_DE#redirecturl

- nodejs + https
https://contextneutral.com/story/creating-an-https-server-with-nodejs-and-express

- Create An HTTPS Server And Run Application HTTPS In Node.js
https://www.c-sharpcorner.com/article/create-an-https-server-and-run-application-https-in-node-js/

- Setup an HTTPS server with NodeJS and Express
https://www.youtube.com/watch?v=8ptiZlO7ROs&t=233s


