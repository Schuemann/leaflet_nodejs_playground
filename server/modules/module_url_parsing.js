var url = require('url'); 

// test-function, parsing url like http://localhost:8080/?year=2017&month=July 
exports.url_parsing = function(req){
    var q = url.parse(req.url /*The Striing to parse*/ , true /*do parse string?*/).query; // parse string like "http://localhost:8080/?year=2017&month=July" and extract its Parameters
	
	if(typeof q.year == 'undefined')
		q.year = ''; // overwrite undefined

	if(typeof q.month == 'undefined')
		q.month = ''; // overwrite undefined
		
	var txt = q.year + " " + q.month; 

    return txt;  
};