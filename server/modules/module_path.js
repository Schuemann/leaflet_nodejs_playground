
// get path to module_date.js

// Paths of JS-Files
exports.path_date_module_js     = './modules/module_date.js';
exports.path_url_parsing_js     = './modules/module_url_parsing.js';
exports.path_fileupload_js      = './modules/module_fileupload.js';

// Paths of HTML-Files
// enum for iterating of ressoruces
exports.ressource_enum = Object.freeze({ 
    'index.html'                        :   1,
    'html_play/html_test.html'          :   2, 
    'html_play/html_test2.html'         :   3,  
    'html_play/html_test_upload.html'   :   4
 });

// explcit ressources for better referencing

var recursive = require('./module_path.js');

exports.index          =            recursive.ressource_enum[1];
exports.path_html_test =            recursive.ressource_enum[2];
exports.path_html_test =            recursive.ressource_enum[3];
exports.path_html_test_upload =     recursive.ressource_enum[4];
// exports.path_leaflet_test_html =    ''; // TODO