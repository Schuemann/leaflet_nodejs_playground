// import this module in a .js-file of choice with "var date_module = require(<relative path from file with 'createServer(...)-function'>)

exports.current_time = function () {
	var h = new Date().getHours();
	var m = new Date().getMinutes();
	if (m < 10) {m = '0' + m;}

	return h + ':' + m;
}; 

exports.current_year = function () {
	return new Date().getFullYear();
}; 
