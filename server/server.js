
var http = require('http'); // using http-module
var path_module = require('./modules/module_path'); // aggregate all paths in one module
var date_module = require(path_module.path_date_module_js); // self written 'library' made methods usable outside its own file
var url = require('url'); // to use url.parse ... 
var fs = require('fs'); // enable working with filesystem -> now possible to use .html-files -> fs.readFile()
var uc = require('upper-case'); // with npm install upper-case installed module
var events = require('events'); // Add nodejs-events to application 
var import_files = require(path_module.path_fileupload_js); // testing fileupload

// finally instantiate server and start listening on port 8080
http.createServer(function (req, res) {

	var q = url.parse(req.url, true);
	var filename = "." + q.pathname;
	console.log('requested ressource: ' + filename);
	console.log('testog ' + path_module.path_html_test);

	// check request vs. available ressources
	for ( let item in path_module.ressource_enum) {
		fs.readFile(filename, function(err, data) {

			if(err) { // return error-msg if no ressource was found
				res.writeHead(404, {'Content-Type': 'text/html'});
				return res.end("404 Not Found");
			}

			if(filename.includes(item)){ 
				res.writeHead(200, {'Content-Type': 'text/html'});
				res.write(data); // serve ressource
				// res.write(uc("Hello World!")); upper-case usage
				return res.end();	
			}
		});	
	}

  }).listen(8080); 
